package main

import (
	"fmt"
	"unicode"
)

func main() {
	r := '\xaa'
	fmt.Printf("%v\n", string(r))
	fmt.Println(unicode.IsPrint(r))
	for _, v := range unicode.Categories {
		for _, s := range v.R16 {
			fmt.Printf("%%x%X-%X\n", s.Lo, s.Hi)
		}
		for _, s := range v.R32 {
			fmt.Printf("%%x%X-%X\n", s.Lo, s.Hi)
		}
	}
}
